/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package de.exxcellent.challenge.data;

/**
 * Class for weather data
 *
 * @author Marian Hieke
 */
public class WeatherData extends Data{
    
    
    private int day = -1;
    private int mxt = -1;
    private int mnt = -1;
    private int avt = -1;
    private double avdp = -1;
    private int oneHrpTpcpn = -1;
    private int pdir = -1;
    private double avsp = -1;
    private int dir = -1;
    private int mxs = -1;
    private double skyc = -1;
    private int mxr = -1;
    private int mn = -1;
    private double rAvslp = -1;

    public WeatherData(String[] header, String[] data) {

        if (header.length != data.length) {
            this.valid = false;
            return;
        }
        if (data.length != 14) {
            this.valid = false;
            return;
        }

        this.day = Integer.valueOf(data[0]);
        this.mxt = Integer.valueOf(data[1]);
        this.mnt = Integer.valueOf(data[2]);
        this.avt = Integer.valueOf(data[3]);
        this.avdp = Double.valueOf(data[4]);
        this.oneHrpTpcpn = Integer.valueOf(data[5]);
        this.pdir = Integer.valueOf(data[6]);
        this.avsp = Double.valueOf(data[7]);
        this.dir = Integer.valueOf(data[8]);
        this.mxs = Integer.valueOf(data[9]);
        this.skyc = Double.valueOf(data[10]);
        this.mxr = Integer.valueOf(data[11]);
        this.mn = Integer.valueOf(data[12]);
        this.rAvslp = Double.valueOf(data[13]);
        this.valid = true;
    }


    /**
     * computes the temparutre spread mxt minus mnt and returns it
     *
     * @return the temperature spread
     */
    public int getTemperatureSpread() {
        return mxt - mnt;

    }

    @Override
    public String getValue() {
        return "" + this.day;
    }
    @Override
    public int getComparissionValue() {
        return this.getTemperatureSpread();
    }
}
