/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package de.exxcellent.challenge.data;

import java.util.LinkedList;

/**
 * Class for collecting data and providing additional functionality
 *
 * @author Marian Hieke
 */
public class DataCollection {

    private LinkedList<Data> dataList;

    
    public DataCollection(LinkedList<Data> dataList) {
        
        this.dataList = dataList;
        
    }

    /**
     * Return the data with the minimum corresponding to the metric for the type
     * of data
     *
     * @return data
     */
    public Data getMinimum() {
        Data data = null;

                
                if(dataList.size() >0){
                    data = dataList.getFirst();
                    //   System.out.println("--------------------------------------------------------");
                    for (Data d : dataList) {
                        //   System.out.println("comparrision: " + d.getComparissionValue());
                        if (data.getComparissionValue() > d.getComparissionValue()) {

                          data = d;
                      }
                  }
                    
                }


        return data;
    }

    /**
     *
     * @return int size, the amount of data contained
     */
    public int getSize(){
        if(this.dataList == null){
            return -1;
        } else {
            return dataList.size();
        }
    }

    /**
     * Checks if the collection contains at least one invalid data object
     *
     * @return true if this collection contains invalid data
     */
    public boolean containsInvalidData() {
        for (Data d : this.dataList) {
            if (!d.isValid()) {
                System.out.println("invalid data: " + d.getValue());
                return true;
            }
        }
        return false;
    }
    

    
}
