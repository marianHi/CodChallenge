/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package de.exxcellent.challenge.data;

/**
 * Class for football data
 *
 * @author Marian Hieke
 */
public class FootballData extends Data{
    
    
    private String team = "";
    private int games = -1;
    private int wins = -1;
    private int losses = -1;
    private int draws = -1;
    private int goals = -1;
    private int goalsAllowed = -1;
    private int points = -1;

    public FootballData(String[] header, String[] data) {

        if (header.length != data.length) {
            this.valid = false;
            return;
        }
        if (data.length != 8) {
            this.valid = false;
            return;
        }

        this.team = data[0];
        this.games = Integer.valueOf(data[1]);
        this.wins = Integer.valueOf(data[2]);
        this.losses = Integer.valueOf(data[3]);
        this.draws = Integer.valueOf(data[4]);
        this.goals = Integer.valueOf(data[5]);
        this.goalsAllowed = Integer.valueOf(data[6]);
        this.points = Integer.valueOf(data[7]);
        this.valid = true;


    }
    /**
     * computes the temparutre spread mxt minus mnt and returns it
     *
     * @return the distance between scored and scored against
     */
    public int getGoalDistance() {
        return Math.abs(goals - goalsAllowed);

    }

    @Override
    public String getValue() {
        return this.team;
    }

    @Override
    public int getComparissionValue() {
        return this.getGoalDistance();
    
    }
}
