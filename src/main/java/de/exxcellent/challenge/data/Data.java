/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package de.exxcellent.challenge.data;

/**
 * Class for repressenting data witrh different values and providing additional
 * functionality
 *
 * @author Marian
 */
public class Data {

    protected boolean valid = false;

    /**
     *
     * @return the identification value of this data
     */
    public String getValue() {
        return "";
    }

    /**
     *
     * @return the comparision value for this data
     */
    public int getComparissionValue() {
        return -1;
    }

    /**
     *
     * @return true if this data is valid, false otherwise
     */
    public boolean isValid() {
        return valid;
    }

    
}
