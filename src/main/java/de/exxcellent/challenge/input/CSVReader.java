package de.exxcellent.challenge.input;

import de.exxcellent.challenge.data.Data;
import de.exxcellent.challenge.data.FootballData;
import de.exxcellent.challenge.data.WeatherData;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 * Class for reading data from a CSV file
 * @author Marian Hieke
 */
public class CSVReader implements Reader {

    private String path;
    
    public CSVReader(String path){
        this.path = path;
        
    }

    @Override
    public LinkedList<Data> read() {

        BufferedReader reader = null;
        LinkedList<Data> dataList = new LinkedList();
        try {
            String row;
            boolean headerLine = true;
            String header[] = null;
            reader = new BufferedReader(new FileReader(new File(path)));
         //  System.out.println("File found");
            while ((row = reader.readLine()) != null) {
             //  System.out.println("row: " + row); 

                String[] data = row.split(",");
               // System.out.println("values per row: " + data.length);
                if (data.length <= 0) {
                    break;
                }
                if (headerLine) {
                    header = data;
                    headerLine = false;
                } else {
                    if (header != null) {
                        switch (header[0]) {
                            case "Day":
                                dataList.add(new WeatherData(header, data));
                                break;
                            case "Team":
                                dataList.add(new FootballData(header, data));
                                break;
                            default:
                                break;
                        }
                    }

                }


            }
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("File not found");
        } catch (IOException ex) {
             System.out.println("IO Exception");
            Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                System.out.println("IO Exception in finally");
                Logger.getLogger(CSVReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return dataList;
    }


    
}
