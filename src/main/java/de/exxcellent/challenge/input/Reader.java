package de.exxcellent.challenge.input;

import de.exxcellent.challenge.data.Data;
import java.util.LinkedList;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 * Class for a reader for reading data in
 *
 * @author Marian Hieke
 */
public interface Reader {

    /**
     * Reads the data in and return it as a linked list
     *
     * @return LinkedList containing the read in data
     */
    public abstract LinkedList<Data> read();
    
}
