/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package de.exxcellent.challenge;

import de.exxcellent.challenge.data.DataCollection;
import de.exxcellent.challenge.input.CSVReader;
import de.exxcellent.challenge.input.Reader;

/**
 * Class for reading the data in and collecting it for the given typeof data
 *
 * @author Marian
 */
public class Runner {

    private String minimum = "";
    private DataCollection col;

    public void run(String path) {

        Reader reader = new CSVReader(path);
        col = new DataCollection(((CSVReader) reader).read());
        if (col.getMinimum() == null) {
            //   System.out.println("col min is null. col size: " + col.getSize());
        } else {
            minimum = col.getMinimum().getValue();
        }


    }

    /**
     *
     * @return string the identification vaue of the minimum data of all found
     * data
     */
    public String getMinimum() {
        return minimum;
    }

    /**
     *
     * @return DatasetCollection col, the collection filled during the run
     */
    public DataCollection getCol() {
        return col;
    }



}
