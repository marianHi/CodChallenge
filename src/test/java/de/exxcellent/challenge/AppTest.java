package de.exxcellent.challenge;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


/**
 * Example JUnit 5 test case.
 * @author Benjamin Schmid <benjamin.schmid@exxcellent.de>
 */
class AppTest {

    private Runner runner;
    private Runner runnerF;

    @BeforeEach
    void setUp() {
        runner = new Runner();

        String path = "src/main/resources/de/exxcellent/challenge/weather.csv";
        runner.run(path);

        runnerF = new Runner();

        String pathF = "src/main/resources/de/exxcellent/challenge/football.csv";
        runnerF.run(pathF);
    }

    @Test
    void weatherTestRsult() {
        assertEquals("14", runner.getMinimum());
    }

    @Test
    void weatherDataSize() {
        assertEquals(30, runner.getCol().getSize());
    }

    @Test
    void weatherMinimumTest() {
        assertEquals(2, runner.getCol().getMinimum().getComparissionValue());
    }

    @Test
    void weatherValidData() {
        assertFalse(runner.getCol().containsInvalidData());
    }

    @Test
    void footballTestRsult() {
        assertEquals("Aston_Villa", runnerF.getMinimum());
    }

    @Test
    void footballDataSize() {
        assertEquals(20, runnerF.getCol().getSize());
    }

    @Test
    void footballMinimumTest() {
        assertEquals(1, runnerF.getCol().getMinimum().getComparissionValue());
    }

    @Test
    void footballValidData() {
        assertFalse(runnerF.getCol().containsInvalidData());
    }

    /* @Test
    void runFootball() {
        App.main("--football", "football.csv");
    }*/

}